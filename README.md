# Base 32 Coder / Encoder

## Installation:

 ```
npm install --save base32codec
 ```

## How to import:

```
// Javascript
const base32codec = require('base32codec');

// ES6, e.g. in Vue.js
import 'base32codec';
```

## Usage:

```
// Encode I70CJ8G1A35 and get it back
var encoded_4648 = base32codec.encode('I70CJ8G1A35'); // default to RFC4648 encoder
var decoded_4648 = base32codec.decode('JE3TAQ2KHBDTCQJTGU======'); // default to RFC4648 decoder

// Other modes supported
var encoded_hex = base32codec.encode('I70CJ8G1A35', 'hex'); // RFC4648_HEX encoder
var encoded_crockford = base32codec.encode('I70CJ8G1A35', 'crockford'); // Crockford encoder
```