(function(self) {
  'use strict';

  if (self.base32codec) {
    return;
  }

  // https://tools.ietf.org/html/rfc4648#page-8
  const rfc4648 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ234567';
  const rfc4648_hex = '0123456789ABCDEFGHIJKLMNOPQRSTUV';
  const crockford = '0123456789ABCDEFGHJKMNPQRSTVWXYZ';

  function readNBits(nBits, text, bitPos) {
    var bytePos = Math.floor(bitPos / 8),
        localBitPos = bitPos % 8;

    if (bytePos >= text.length) {
      return {
        status: 'overflow'
      };
    }

    var byteCode = text.charCodeAt(bytePos),
        readableBits = 8 - localBitPos,
        alignment = 8 - nBits;

    var result = '';
    if (readableBits >= nBits) {
      result = ((byteCode << localBitPos) & 0xFF) >>> alignment;
      bitPos += nBits;
    } else {
      var missingBits = nBits - readableBits,
          _result = readNBits(missingBits, text, bitPos + readableBits);

      if (_result.status === 'ok') {
        result = ((byteCode << localBitPos) & 0xFF) >>> alignment;
        result |= _result.result;
        bitPos = _result.bitPos;
      } else {
        return {
          status: 'incomplete',
          result: ((byteCode << localBitPos) & 0xFF) >>> alignment,
          missingBits: missingBits
        }
      }
    }

    return {
      status: 'ok',
      result: result,
      bitPos: bitPos
    };
  }

  self.base32codec = {
    encode: function (text, mode) {
      var pattern, padding;
      switch (mode) {
      case 'hex':
        pattern = rfc4648_hex;
        padding = true;
        break
      case 'crockford':
        pattern = crockford;
        break
      default:
        // default to RFC3548 / RFC4648
        pattern = rfc4648;
        padding = true;
        break
      }

      var encoded = '',
          bitPos = 0;

      while (true) {
        var result = readNBits(5, text, bitPos);
        if (result.status === 'ok') {
          encoded += pattern[result.result];
          bitPos = result.bitPos;
        } else {
          if (result.status === 'incomplete') {
            encoded += pattern[result.result];
          }
          break;
        }
      }

      if (padding) {
        while ((encoded.length % 8) !== 0) {
          encoded += '=';
        }
      }
      return encoded;
    },
    decode: function (encoded, mode) {
      var pattern;
      switch (mode) {
      case 'hex':
        pattern = rfc4648_hex;
        encoded = encoded.replace(/=+$/, '');
        break
      case 'crockford':
        pattern = crockford;
        encoded = encoded.toUpperCase().replace(/O/g, '0').replace(/[IL]/g, '1');
        break
      default:
        // default to RFC3548 / RFC4648
        pattern = rfc4648;
        encoded = encoded.replace(/=+$/, '');
        break
      }

      var requiredBits = 8,
          partialVal = 0,
          decoded = '';
      for (var i = 0; i < encoded.length; i++) {
        var bytePos = pattern.indexOf(encoded[i]);

        if (bytePos === -1) {
          throw new Error('Invalid letter: ' + encoded[i]);
        }

        if (requiredBits >= 5) {
          partialVal |= (bytePos << (requiredBits - 5)) & 0xFF;
          requiredBits -= 5;
        } else {
          var carryOverBits = 5 - requiredBits;
          partialVal |= bytePos >>> carryOverBits;
          decoded += String.fromCharCode(partialVal);
          var nextRequiredBits = 8 - carryOverBits;
          partialVal = (bytePos << nextRequiredBits) & 0xFF;
          requiredBits = nextRequiredBits;
        }
      }

      return decoded;
    }
  };
})(typeof self !== 'undefined' ? self : this);
